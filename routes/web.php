<?php

use App\Http\Controllers\GenerateController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ScanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class, 'index'])->name('home');

Route::prefix('generate')->name('generate.')->group(function(){

    Route::get('/',[GenerateController::class, 'index'])->name('index');
    Route::post('/create',[GenerateController::class, 'create'])->name('create');
    

});

Route::prefix('scanner')->name('scanner.')->group(function(){

    Route::get('/',[ScanController::class, 'index'])->name('index');
    Route::get('/scan',[ScanController::class, 'scan'])->name('scan');

});



