<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <title>Laravel</title>
        <link rel="canonical" href="https://serratus.github.io/examples/live_w_locator.html">
        <link rel="stylesheet" href="https://serratus.github.io/quaggaJS/stylesheets/styles.css">
        <link rel="stylesheet" href="https://serratus.github.io/quaggaJS/stylesheets/example.css">
        <link rel="stylesheet" href="https://serratus.github.io/quaggaJS/stylesheets/pygment_trac.css">
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="//webrtc.github.io/adapter/adapter-latest.js" type="text/javascript"></script>
        <script src="https://serratus.github.io/quaggaJS/javascripts/scale.fix.js"></script>
        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            var host = "serratus.github.io";
            if ((host == window.location.host) && (window.location.protocol != "https:")) {
                window.location.protocol = "https";
            }
        </script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      
            ga('create', 'UA-56318310-1', 'auto');
            ga('send', 'pageview');
      
          </script>
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <script  src="{{ asset('dist/quagga.min.js')}}"></script>
        <div class="controls">
            <fieldset class="input-group">
                <button class="stop"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Arrêt</font></font></button>
            </fieldset>
            <fieldset class="reader-config-group">
                
                <label>
                    <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Résolution (côté long)</font></font></span>
                    <select name="input-stream_constraints">
                        <option value="320x240"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">320px</font></font></option>
                        <option selected="selected" value="640x480"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">640px</font></font></option>
                        <option value="800x600"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">800px</font></font></option>
                        <option value="1280x720"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1280px</font></font></option>
                        <option value="1600x960"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1600px</font></font></option>
                        <option value="1920x1080"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1920px</font></font></option>
                    </select>
                </label>
                <label>
                    <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Taille du patch</font></font></span>
                    <select name="locator_patch-size">
                        <option value="x-small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">x-petit</font></font></option>
                        <option value="small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">petit</font></font></option>
                        <option selected="selected" value="medium"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">moyen</font></font></option>
                        <option value="large"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">grand</font></font></option>
                        <option value="x-large"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">x-large</font></font></option>
                    </select>
                </label>
                <label>
                    <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Demi-échantillon</font></font></span>
                    <input type="checkbox" checked="checked" name="locator_half-sample">
                </label>
                <label>
                    <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ouvriers</font></font></span>
                    <select name="numOfWorkers">
                        <option value="0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0</font></font></option>
                        <option value="1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1</font></font></option>
                        <option value="2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2</font></font></option>
                        <option selected="selected" value="4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4</font></font></option>
                        <option value="8"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">8</font></font></option>
                    </select>
                </label>
                <label>
                    <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Caméra</font></font></span>
                    <select name="input-stream_constraints" id="deviceSelection"><option value="ccccaba053b9cbe6e7e9f1092e6f848314588eab6b997272848330cb7b45ef02">HP HD Webcam (04f2:b3ed)</option></select>
                </label>
                <label style="display: none">
                    <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Zoom</font></font></span>
                    <select name="settings_zoom"></select>
                </label>
                <label style="display: none">
                    <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Torche</font></font></span>
                    <input type="checkbox" name="settings_torch">
                </label>
            </fieldset>
        </div>
        <div id="result_strip">
            <ul class="thumbnails"></ul>
        </div>
        <div id="interactive" class="viewport">
            <video autoplay="true" preload="auto" src="" muted="true" playsinline="true">
                </video><canvas class="drawingBuffer" width="640" height="480"></canvas>
                <br clear="all"></div>
    <script src="https://serratus.github.io/quaggaJS/javascripts/scale.fix.js"></script>
    <script >
    $(function() {
    var App = {
        init : function() {
            Quagga.init(this.state, function(err) {
                if (err) {
                    console.log(err);
                    return;
                }
                App.attachListeners();
                App.checkCapabilities();
                Quagga.start();
            });
        },
        checkCapabilities: function() {
            var track = Quagga.CameraAccess.getActiveTrack();
            var capabilities = {};
            if (typeof track.getCapabilities === 'function') {
                capabilities = track.getCapabilities();
            }
            this.applySettingsVisibility('zoom', capabilities.zoom);
            this.applySettingsVisibility('torch', capabilities.torch);
        },
        updateOptionsForMediaRange: function(node, range) {
            console.log('updateOptionsForMediaRange', node, range);
            var NUM_STEPS = 6;
            var stepSize = (range.max - range.min) / NUM_STEPS;
            var option;
            var value;
            while (node.firstChild) {
                node.removeChild(node.firstChild);
            }
            for (var i = 0; i <= NUM_STEPS; i++) {
                value = range.min + (stepSize * i);
                option = document.createElement('option');
                option.value = value;
                option.innerHTML = value;
                node.appendChild(option);
            }
        },
        applySettingsVisibility: function(setting, capability) {
            // depending on type of capability
            if (typeof capability === 'boolean') {
                var node = document.querySelector('input[name="settings_' + setting + '"]');
                if (node) {
                    node.parentNode.style.display = capability ? 'block' : 'none';
                }
                return;
            }
            if (window.MediaSettingsRange && capability instanceof window.MediaSettingsRange) {
                var node = document.querySelector('select[name="settings_' + setting + '"]');
                if (node) {
                    this.updateOptionsForMediaRange(node, capability);
                    node.parentNode.style.display = 'block';
                }
                return;
            }
        },
        initCameraSelection: function(){
            var streamLabel = Quagga.CameraAccess.getActiveStreamLabel();

            return Quagga.CameraAccess.enumerateVideoDevices()
            .then(function(devices) {
                function pruneText(text) {
                    return text.length > 30 ? text.substr(0, 30) : text;
                }
                var $deviceSelection = document.getElementById("deviceSelection");
                while ($deviceSelection.firstChild) {
                    $deviceSelection.removeChild($deviceSelection.firstChild);
                }
                devices.forEach(function(device) {
                    var $option = document.createElement("option");
                    $option.value = device.deviceId || device.id;
                    $option.appendChild(document.createTextNode(pruneText(device.label || device.deviceId || device.id)));
                    $option.selected = streamLabel === device.label;
                    $deviceSelection.appendChild($option);
                });
            });
        },
        attachListeners: function() {
            var self = this;

            self.initCameraSelection();
            $(".controls").on("click", "button.stop", function(e) {
                e.preventDefault();
                Quagga.stop();
            });

            $(".controls .reader-config-group").on("change", "input, select", function(e) {
                e.preventDefault();
                var $target = $(e.target),
                    value = $target.attr("type") === "checkbox" ? $target.prop("checked") : $target.val(),
                    name = $target.attr("name"),
                    state = self._convertNameToState(name);

                console.log("Value of "+ state + " changed to " + value);
                self.setState(state, value);
            });
        },
        _accessByPath: function(obj, path, val) {
            var parts = path.split('.'),
                depth = parts.length,
                setter = (typeof val !== "undefined") ? true : false;

            return parts.reduce(function(o, key, i) {
                if (setter && (i + 1) === depth) {
                    if (typeof o[key] === "object" && typeof val === "object") {
                        Object.assign(o[key], val);
                    } else {
                        o[key] = val;
                    }
                }
                return key in o ? o[key] : {};
            }, obj);
        },
        _convertNameToState: function(name) {
            return name.replace("_", ".").split("-").reduce(function(result, value) {
                return result + value.charAt(0).toUpperCase() + value.substring(1);
            });
        },
        detachListeners: function() {
            $(".controls").off("click", "button.stop");
            $(".controls .reader-config-group").off("change", "input, select");
        },
        applySetting: function(setting, value) {
            var track = Quagga.CameraAccess.getActiveTrack();
            if (track && typeof track.getCapabilities === 'function') {
                switch (setting) {
                case 'zoom':
                    return track.applyConstraints({advanced: [{zoom: parseFloat(value)}]});
                case 'torch':
                    return track.applyConstraints({advanced: [{torch: !!value}]});
                }
            }
        },
        setState: function(path, value) {
            var self = this;

            if (typeof self._accessByPath(self.inputMapper, path) === "function") {
                value = self._accessByPath(self.inputMapper, path)(value);
            }

            if (path.startsWith('settings.')) {
                var setting = path.substring(9);
                return self.applySetting(setting, value);
            }
            self._accessByPath(self.state, path, value);

            console.log(JSON.stringify(self.state));
            App.detachListeners();
            Quagga.stop();
            App.init();
        },
        inputMapper: {
            inputStream: {
                constraints: function(value){
                    if (/^(\d+)x(\d+)$/.test(value)) {
                        var values = value.split('x');
                        return {
                            width: {min: parseInt(values[0])},
                            height: {min: parseInt(values[1])}
                        };
                    }
                    return {
                        deviceId: value
                    };
                }
            },
            numOfWorkers: function(value) {
                return parseInt(value);
            },
            decoder: {
                readers: function(value) {
                    if (value === 'ean_extended') {
                        return [{
                            format: "ean_reader",
                            config: {
                                supplements: [
                                    'ean_5_reader', 'ean_2_reader'
                                ]
                            }
                        }];
                    }
                    return [{
                        format: value + "_reader",
                        config: {}
                    }];
                }
            }
        },
        state: {
            inputStream: {
                type : "LiveStream",
                constraints: {
                    width: {min: 640},
                    height: {min: 480},
                    aspectRatio: {min: 1, max: 100},
                    facingMode: "environment" // or user
                }
            },
            locator: {
                patchSize: "medium",
                halfSample: true
            },
            numOfWorkers: 2,
            frequency: 10,
            decoder: {
                readers : [{
                    format: "codabar_reader",
                    config: {}
                }]
            },
            locate: true
        },
        lastResult : null
    };

    App.init();

    Quagga.onProcessed(function(result) {
        
        var drawingCtx = Quagga.canvas.ctx.overlay,
            drawingCanvas = Quagga.canvas.dom.overlay;

        if (result) {
            
            if (result.boxes) {
                drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
                result.boxes.filter(function (box) {
                    return box !== result.box;
                }).forEach(function (box) {
                    Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, {color: "green", lineWidth: 2});
                });
            }

            if (result.box) {
                Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, {color: "#00F", lineWidth: 2});
            }

            if (result.codeResult && result.codeResult.code) {
            
                Quagga.ImageDebug.drawPath(result.line, {x: 'x', y: 'y'}, drawingCtx, {color: 'red', lineWidth: 3});
            }
        }
    });

    Quagga.onDetected(function(result) {
        var code = result.codeResult.code;

        if (App.lastResult !== code) {
            App.lastResult = code;
            var $node = null, canvas = Quagga.canvas.dom.image;

            $node = $('<li><div class="thumbnail"><div class="imgWrapper"><img /></div><div class="caption"><h4 class="code"></h4></div></div></li>');
            $node.find("img").attr("src", canvas.toDataURL());
            $node.find("h4.code").html(code);
            $("#result_strip ul.thumbnails").prepend($node);
        }
    });
});

    </script>
    <script src="https://serratus.github.io/quaggaJS/examples/js/quagga.min.js" type="text/javascript"></script>
   
    </body>
</html>