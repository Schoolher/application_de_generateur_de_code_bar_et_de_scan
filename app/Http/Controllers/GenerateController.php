<?php

namespace App\Http\Controllers;

use App\Models\CodeBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\UrlGeneration\PublicUrlGenerator;
use Milon\Barcode\DNS1D;
use Milon\Barcode\DNS2D;

class GenerateController extends Controller
{
    public function index()
    {
        return view('generate.index');
    }
    public function create(Request $request)
    {
        $request->validate([
            'iden'=>'required|digits:9'
        ]);

        $data = $request->except('_token');
        
        $d = new DNS1D();
        $image = $d->getBarcodePNG($data['iden'], 'CODABAR');
        $base64decode = base64_decode($image);
        $data['code'] = $image;
        $codename = $data['iden'];
        CodeBar::create($data) ;

        Storage::disk('public')->put("CodeBar/".$codename.".png", $base64decode);
        return view('generate.index',compact('codename'));
    }

}

